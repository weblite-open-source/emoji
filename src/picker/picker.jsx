import React from 'react'
import Paper from 'material-ui/Paper'

import EmojiElems from './emojiElements'
import EmojiNavigator from './emojiCategoryNavigator'

const styles = {
  root: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    zIndex: 90000,
  },
  base: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  panel: {
    height: 350,
    overflow: 'hidden',
    zIndex: 100000,
    width: 300,
  },
}

class Emoji extends React.Component {
  constructor(props) {
    super(props)
    this.state = { type: 'People', index: 0 }
    this._setState = this._setState.bind(this)
  }

  _setState(payload) {
    this.setState(payload)
  }

  render() {
    if (!this.props.isOpen) return null
    return (
      <div style={styles.root} >
        {/* Div behind the panel in order to close the panel in case of click out side of it */}
        <div
          style={styles.base}
          onClick={this.props.closeEmoji}
        />
        {/* Emoji picker panel */}
        <Paper
          style={{
            ...styles.panel,
            ...this.props.style,
          }}
        >
          <EmojiElems
            type={this.state.type}
            addEmoji={this.props.addEmoji}
          />
          <EmojiNavigator
            currentIndex={this.state.index}
            setState={this._setState}
            width={this.props.style.width}
          />
        </Paper>
      </div>
    )
  }
}

Emoji.propTypes = {
  isOpen: React.PropTypes.bool,
  style: React.PropTypes.shape({
    width: React.PropTypes.number,
  }),
  closeEmoji: React.PropTypes.func.isRequired,
  addEmoji: React.PropTypes.func.isRequired,
}

Emoji.defaultProps = {
  isOpen: false,
  style: {
    width: 300,
  },
}

export default Emoji
