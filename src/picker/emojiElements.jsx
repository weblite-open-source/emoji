import React from 'react'
import IconButton from 'material-ui/IconButton'

import { getEmojisFromCategory } from '../utility/utilities'

const styles = {
  root: {
    position: 'relative',
    height: 297,
    overflow: 'scroll',
  },
  emoji: {
    display: 'inline-block',
    width: 30,
    height: 30,
    paddingLeft: 5,
    paddingTop: 5,
  },
  emojiHeader: {
    paddingTop: 5,
    paddingLeft: 5,
    color: '#9E9E9E',
  },
}

const EmojiElems = ({ type, addEmoji }) => (
  <div style={styles.root}>
    <div style={styles.emojiHeader}>{type}</div>
    {getEmojisFromCategory(`${type}`).map(emoji => (
      <IconButton
        key={emoji[0]}
        onClick={() => addEmoji(emoji[0])}
        style={styles.emoji}
      >
        <img
          width="20"
          height="20"
          src={`http://cdn.jsdelivr.net/emojione/assets/svg/${emoji[0]}.svg?v=2.2.6`}
          alt={type}
        />
      </IconButton>
    ))}
  </div>
)

EmojiElems.propTypes = {
  type: React.PropTypes.string,
  addEmoji: React.PropTypes.func.isRequired,
}

EmojiElems.defaultProps = {
  type: 'nature',
}

export default EmojiElems
