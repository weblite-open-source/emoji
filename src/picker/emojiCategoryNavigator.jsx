import React from 'react'
import Paper from 'material-ui/Paper'
import { BottomNavigation, BottomNavigationItem } from 'material-ui/BottomNavigation'

import PeopleEmoji from 'material-ui/svg-icons/editor/insert-emoticon'
import ObjectsEmoji from 'material-ui/svg-icons/places/beach-access'
import ActivityEmoji from 'material-ui/svg-icons/maps/directions-bike'
import NatureEmoji from 'material-ui/svg-icons/maps/local-florist'
import TravelEmoji from 'material-ui/svg-icons/maps/directions-boat'
import SymbolEmoji from 'material-ui/svg-icons/action/favorite-border'
import FoodEmoji from 'material-ui/svg-icons/maps/local-dining'
import FlagEmoji from 'material-ui/svg-icons/social/public'

const styles = {
  emojiPaperNavBar: {
    position: 'relative',
    height: 48,
  },
  navigationButton: {
    width: 37.5,
    minWidth: 13,
    height: 50,
    paddingRight: 0,
    paddingTop: 2,
    paddingLeft: 0,
    paddingBottom: 0,
  },
}

const logoTypes = [
  <PeopleEmoji />,
  <ObjectsEmoji />,
  <ActivityEmoji />,
  <NatureEmoji />,
  <TravelEmoji />,
  <SymbolEmoji />,
  <FoodEmoji />,
  <FlagEmoji />,
]

const EmojiNavigator = ({ currentIndex, setState, width }) => (
  <Paper
    style={{ ...styles.emojiPaperNavBar, width }}
  >
    <BottomNavigation
      selectedIndex={currentIndex}
      style={styles.navigationBar}
    >
      {
        ['People', 'Objects', 'Activity', 'Nature', 'Travel', 'Symbols', 'Food', 'Flags'].map(
          (type, index) => (
            <BottomNavigationItem
              style={styles.navigationButton}
              key={index}
              icon={logoTypes[index]}
              onTouchTap={() => setState({ type, index })}
            />
          ),
        )
      }
    </BottomNavigation>
  </Paper>
)


EmojiNavigator.propTypes = {
  currentIndex: React.PropTypes.number,
  setState: React.PropTypes.func.isRequired,
  width: React.PropTypes.number,
}

EmojiNavigator.defaultProps = {
  currentIndex: 0,
  width: 300,
}

export default EmojiNavigator
