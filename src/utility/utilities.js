// @flow

// modules
import R from 'ramda'
// emoji db
import emojis from '../db/emojiDB'


// type aliases
type EmojiCharacter = string
type EmojiUnicode = string
type EmojiRow = [EmojiCharacter, EmojiUnicode]


const flattenEmojiDB = (): Array<EmojiRow> =>
  R.values(emojis).reduce((val, acc) => [...acc, ...val], [])

const getEmojiChar = (unicode: string): string =>
  R.find(R.propEq(0, unicode))(flattenEmojiDB())[1]

const getEmojisFromCategory = (cat: string): EmojiRow => emojis[cat]

const hasEmoji = (emoji: string): ?EmojiRow =>
  R.find(R.propEq(1, emoji))(flattenEmojiDB())

export { getEmojiChar, getEmojisFromCategory, hasEmoji }
