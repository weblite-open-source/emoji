import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import injectTapEventPlugin from 'react-tap-event-plugin'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

import Picker from './picker/picker'

injectTapEventPlugin()

const render = () => {
  ReactDOM.render(
    <AppContainer>
      <MuiThemeProvider>
        <Picker
          isOpen
          style={{
            margin: '0 auto',
            marginTop: 30,
          }}
          closeEmoji={() => console.log('close')}
          addEmoji={emj => console.log(emj)}
        />
      </MuiThemeProvider>
    </AppContainer>,
    document.getElementById('root'),
  )
}

render()

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./picker/picker', () => {
    render()
  })
}
