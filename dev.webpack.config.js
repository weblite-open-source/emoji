const { resolve } = require('path');
const webpack = require('webpack');

var FlowStatusWebpackPlugin = require('flow-status-webpack-plugin');
var DashboardPlugin = require('webpack-dashboard/plugin');

module.exports = function(env) {
  return {
    context: resolve(__dirname + '/src'),

    entry: {
        app: [
          'react-hot-loader/patch',
          'webpack-dev-server/client?http://localhost:8081',
          'webpack/hot/only-dev-server',
          './dev.index.jsx'
        ],
    },

    output: {
      filename: '[name].js',
      path: resolve(__dirname, 'dist'),
      publicPath: '/'
    },

    devtool: 'eval',

    devServer: {
      hot: true,
      contentBase: resolve(__dirname, 'dist'),
      publicPath: '/',
      historyApiFallback: true,
    },

    resolve: {
      extensions: [".js", ".jsx"]
    },

    module: {
      rules: [
        {
          enforce: "pre",
          test: /\.(jsx|js)$/,
          exclude: /node_modules/,
          loader: "eslint-loader",
        },
        {
          test: /\.(jsx|js)?$/,
          use: [ 'babel-loader', ],
          exclude: /node_modules/
        },
      ],
    },

    plugins: [
      new webpack.HotModuleReplacementPlugin(),

      new webpack.NamedModulesPlugin(),

      new FlowStatusWebpackPlugin({
        failOnError: true,
      }),

      new DashboardPlugin({ port: 9882 }),
    ],
  }
};
