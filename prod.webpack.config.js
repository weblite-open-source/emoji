const { resolve } = require('path');
const webpack = require('webpack');

var FlowStatusWebpackPlugin = require('flow-status-webpack-plugin');
var DashboardPlugin = require('webpack-dashboard/plugin');

module.exports = function(env) {
  return {
    context: resolve(__dirname + '/src'),

    entry: {
        index: './prod.index.js',
    },

    output: {
      filename: '[name].js',
      path: resolve(__dirname, 'dist'),
      publicPath: '/',
      libraryTarget: 'umd',
    },

    devtool: 'nosources-source-map',

    resolve: {
      extensions: [".js", ".jsx"]
    },

    module: {
      rules: [
        {
          enforce: "pre",
          test: /\.(jsx|js)$/,
          exclude: /node_modules/,
          loader: "eslint-loader",
        },
        {
          test: /\.(jsx|js)?$/,
          use: [ 'babel-loader', ],
          exclude: /node_modules/
        },
      ],
    },

    externals: {
      "react": {
        commonjs: "react",
        commonjs2: "react",
        amd: "react",
        umd: "react",
        root: "React",
      },
      "react-dom": {
        commonjs: "react-dom",
        commonjs2: "react-dom",
        amd: "react-dom",
        umd: "react-dom",
        root: "ReactDom",
      },
      "ramda": {
        commonjs: "ramda",
        commonjs2: "ramda",
        amd: "ramda",
        umd: "ramda",
        root: "R",
      },
      "material-ui/Paper": {
        commonjs: "material-ui/Paper",
        commonjs2: "material-ui/Paper",
        amd: "material-ui/Paper",
        umd: "material-ui/Paper",
      },
      "material-ui/IconButton": {
        commonjs: "material-ui/IconButton",
        commonjs2: "material-ui/IconButton",
        amd: "material-ui/IconButton",
        umd: "material-ui/IconButton",
      },
      "material-ui/BottomNavigation": {
        commonjs: "material-ui/BottomNavigation",
        commonjs2: "material-ui/BottomNavigation",
        amd: "material-ui/BottomNavigation",
        umd: "material-ui/BottomNavigation",
      },
      "material-ui/svg-icons/editor/insert-emoticon": {
        commonjs: "material-ui/svg-icons/editor/insert-emoticon",
        commonjs2: "material-ui/svg-icons/editor/insert-emoticon",
        amd: "material-ui/svg-icons/editor/insert-emoticon",
        umd: "material-ui/svg-icons/editor/insert-emoticon",
      },
      "material-ui/svg-icons/places/beach-access": {
        commonjs: "material-ui/svg-icons/places/beach-access",
        commonjs2: "material-ui/svg-icons/places/beach-access",
        amd: "material-ui/svg-icons/places/beach-access",
        umd: "material-ui/svg-icons/places/beach-access",
      },
      "material-ui/svg-icons/maps/directions-bike": {
        commonjs: "material-ui/svg-icons/maps/directions-bike",
        commonjs2: "material-ui/svg-icons/maps/directions-bike",
        amd: "material-ui/svg-icons/maps/directions-bike",
        umd: "material-ui/svg-icons/maps/directions-bike",
      },
      "material-ui/svg-icons/maps/local-florist": {
        commonjs: "material-ui/svg-icons/maps/local-florist",
        commonjs2: "material-ui/svg-icons/maps/local-florist",
        amd: "material-ui/svg-icons/maps/local-florist",
        umd: "material-ui/svg-icons/maps/local-florist",
      },
      "material-ui/svg-icons/maps/directions-boat": {
        commonjs: "material-ui/svg-icons/maps/directions-boat",
        commonjs2: "material-ui/svg-icons/maps/directions-boat",
        amd: "material-ui/svg-icons/maps/directions-boat",
        umd: "material-ui/svg-icons/maps/directions-boat",
      },
      "material-ui/svg-icons/action/favorite-border": {
        commonjs: "material-ui/svg-icons/action/favorite-border",
        commonjs2: "material-ui/svg-icons/action/favorite-border",
        amd: "material-ui/svg-icons/action/favorite-border",
        umd: "material-ui/svg-icons/action/favorite-border",
      },
      "material-ui/svg-icons/maps/local-dining": {
        commonjs: "material-ui/svg-icons/maps/local-dining",
        commonjs2: "material-ui/svg-icons/maps/local-dining",
        amd: "material-ui/svg-icons/maps/local-dining",
        umd: "material-ui/svg-icons/maps/local-dining",
      },
      "material-ui/svg-icons/social/public": {
        commonjs: "material-ui/svg-icons/social/public",
        commonjs2: "material-ui/svg-icons/social/public",
        amd: "material-ui/svg-icons/social/public",
        umd: "material-ui/svg-icons/social/public",
      },
    },

    plugins: [
      new webpack.HotModuleReplacementPlugin(),

      new webpack.NamedModulesPlugin(),

      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),

      new FlowStatusWebpackPlugin({
        failOnError: true,
      }),

      new DashboardPlugin({ port: 9882 }),
    ],
  }
};
